package edu.dctgkk.ws.json;

public class FolderCreateJSON {

	String name;
	Long parentId;
	
	public FolderCreateJSON()
	{
		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	
}
