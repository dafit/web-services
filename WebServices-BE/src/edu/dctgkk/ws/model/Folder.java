package edu.dctgkk.ws.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;

@Entity
public class Folder {
	@Id
	Long ID = null;
	String name;
	Key<User> owner;
	List<Key<Metafile>> metafiles;
	List<Key<Folder>> subfolders;

	Date creationTime;
	boolean isModifiable;

	public Folder() {
		metafiles = new ArrayList<Key<Metafile>>();
		subfolders = new ArrayList<Key<Folder>>();
		creationTime = new Date();
		isModifiable = true;
	}

	public Key<User> getOwner() {
		return owner;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public List<Key<Metafile>> getMetafiles() {
		return metafiles;
	}

	public List<Key<Folder>> getSubfolders() {
		return subfolders;
	}

	public int getSubfoldersSize() {
		return subfolders.size();
	}

	public int getMetafilesSize() {
		return metafiles.size();
	}	

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public void setOwner(Key<User> owner) {
		this.owner = owner;
	}

	public boolean isModifiable() {
		return isModifiable;
	}

	public void setModifiable(boolean isModifiable) {
		this.isModifiable = isModifiable;
	}	
}
