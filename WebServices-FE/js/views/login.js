/**
 * Created by Dawid on 2015-01-25.
 */
define(['jQuery', 'backbone', 'handlebars', 'text!templates/login-template.html', 'common'], function ($, Backbone, Handlebars, loginTemplate, Common) {

    var LoginView = Backbone.View.extend({

        template: Handlebars.compile(loginTemplate),

        ready: {login: false, password: false},

        el: '#main-container',

        events: {
            'click #login-btn': 'login',
            'click #register-btn': 'register',
            'change #inputLogin': 'validateLogin',
            'change #inputPassword': 'validateLogin'
        },

        initialize: function () {
            $("#navbar").hide();
            this.render();
        },

        render: function () {
            this.$el.html(this.template());
            this.$login = this.$("#inputLogin");
            this.$password = this.$("#inputPassword");
            this.$submitButton = this.$("#login-btn");

            this.$submitButton.attr('disabled', 'disabled');
        },

        validateLogin: function (e) {
            if (e.target.id.toUpperCase().indexOf("LOGIN") > -1) {
                this.ready.login = e.target.value.length > 3 ? true : false;
            }
            else if (e.target.id.toUpperCase().indexOf("PASSWORD") > -1) {
                this.ready.password = e.target.value.length > 3 ? true : false;
            }

            if (this.ready.login && this.ready.password) {
                this.$submitButton.removeAttr("disabled");
            }
            else {
                this.$submitButton.attr('disabled', 'disabled');
            }
        },

        register: function (e) {
            e.preventDefault();
            this.goTo("register");
        },

        login: function (e) {
            e.preventDefault();

            var _self = this, data = {login: this.$login.val(), password: this.$password.val()},
                success = function (data, statusCode, request) {
                    Common.setToken(request.getResponseHeader("Token"));
                    Common.setUID(request.getResponseHeader("UID"));
                    Common.setHeadId(request.getResponseHeader("HeadId"));
                    _self.goTo("files");
                };

            $.ajax({
                url: Common.LoginURL,
                method: 'POST',
                data: JSON.stringify(data),
                contentType: "application/json",
                success: success,
                error: Common.defaultError
            });
        }

    });
    return LoginView;

});