package edu.dctgkk.ws.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Query;

import edu.dctgkk.ws.model.File;
import edu.dctgkk.ws.model.Folder;
import edu.dctgkk.ws.model.Metafile;
import edu.dctgkk.ws.model.User;

public class DBDAO {
	public DBDAO() {

	}

	static {
		ObjectifyService.register(File.class);
		ObjectifyService.register(Folder.class);
		ObjectifyService.register(Metafile.class);
		ObjectifyService.register(User.class);
	}
	
	/* ==== USERS ==== */
	public List<User> getAllUsers()
	{
		Objectify ofy = ObjectifyService.begin();
		Query<User> p = ofy.query(User.class);
		return p.list();
	}
	
	public User getUser(Long id)
	{
		Objectify ofy = ObjectifyService.begin();
		User p = ofy.query(User.class).filter("ID", id).get();
		return p;
	}
	
	public User getUser(String login)
	{
		Objectify ofy = ObjectifyService.begin();
		User p = ofy.query(User.class).filter("login", login).get();
		if(p == null)
			return null;
		else
			return p;
	}
	
	public boolean isLoginAvailable(String login)
	{
		Objectify ofy = ObjectifyService.begin();
		User p = ofy.query(User.class).filter("login", login).get();
		
		if(p == null)
			return true;
		else
			return false;
	}
	
	public boolean isPasswordCorrect(String login, String hashPassoword)
	{
		Objectify ofy = ObjectifyService.begin();
		User p = ofy.query(User.class).filter("login", login).get();
		
		if(p == null)
			return false;
		else if(hashPassoword.equals(p.getPassword()))
			return true;
		else
			return false;
	}
	
	public boolean isLoggedIn(Long UID, String token)
	{
		Objectify ofy = ObjectifyService.begin();
		User p = ofy.query(User.class).filter("ID", UID).get();
		
		if(p == null)
			return false;
		else if(token.equals(p.getToken()) && new Date().before(p.getUserSessionExpirationDate()))
			return true;
		else
			return false;
	}
	
	/** Creates or updates user **/
	public void postUser(User user) {
		Objectify ofy = ObjectifyService.begin();
		ofy.put(user);
	}
	
	public void deleteUser(User u)
	{
		Objectify ofy = ObjectifyService.begin();
		ofy.delete(u);
	}
	
	/* ==== FOLDERS ==== */
	public List<Folder> getAllFolders(User user)
	{
		Objectify ofy = ObjectifyService.begin();
		Query<Folder> p = ofy.query(Folder.class).filter("owner", user);
		return p.list();
	}
	
	public Folder getFolderByOwnerID(User user)
	{
		Objectify ofy = ObjectifyService.begin();
		Folder p = ofy.query(Folder.class).filter("owner", user).get();
		return p;
	}
	
	public Folder getFolderByID(Long id)
	{
		Objectify ofy = ObjectifyService.begin();
		Folder p = ofy.query(Folder.class).filter("ID", id).get();
		return p;
	}
	
	public boolean isFolderNameAvailable(String name)
	{
		Objectify ofy = ObjectifyService.begin();
		Folder f = ofy.query(Folder.class).filter("name", name).get();
		
		if(f == null)
			return true;
		else
			return false;
	}
	
	public void deleteFolder(Folder f)
	{
		Objectify ofy = ObjectifyService.begin();
		ofy.delete(f);
	}
	
	/** Creates or updates folder **/
	public void postFolder(Folder folder) {
		Objectify ofy = ObjectifyService.begin();
		ofy.put(folder);
	}
	
	/* ==== METAFILES ==== */
	
	/*public List<Metafile> getAllMetafiles()
	{
		Objectify ofy = ObjectifyService.begin();
		Query<Metafile> p = ofy.query(Metafile.class);
		return p.list();
	}*/
	
	public boolean isMetafileNameAvailable(String name)
	{
		Objectify ofy = ObjectifyService.begin();
		Metafile f = ofy.query(Metafile.class).filter("name", name).get();
		
		if(f == null)
			return true;
		else
			return false;
	}
	
	/** Creates or updates metafile **/
	public void postMetafile(Metafile file) {
		Objectify ofy = ObjectifyService.begin();
		ofy.put(file);
	}
	
	public Metafile getMetafileByID(Long id)
	{
		Objectify ofy = ObjectifyService.begin();
		Metafile p = ofy.query(Metafile.class).filter("ID", id).get();
		return p;
	}
	
	public Metafile getMetafileByName(String name)
    {
        Objectify ofy = ObjectifyService.begin();
        Metafile p = ofy.query(Metafile.class).filter("name", name).get();
        return p;
    }
	
	public void deleteFileAndMetafileById(Long id)
	{
		Objectify ofy = ObjectifyService.begin();
		Metafile metafile = ofy.query(Metafile.class).filter("ID", id).get();
		File file = ofy.query(File.class).filter("ID", metafile.getFileID()).get();
		ofy.delete(metafile);
		ofy.delete(file);
	}
	
	/* ==== FILES ==== */
	public File getFileByID(Long id)
	{
		Objectify ofy = ObjectifyService.begin();
		File p = ofy.query(File.class).filter("ID", id).get();
		return p;
	}
	
	/** Creates or updates file **/
	public void postFile(File file) {
		Objectify ofy = ObjectifyService.begin();
		ofy.put(file);
	}
	
	public void deleteFile(File u)
	{
		Objectify ofy = ObjectifyService.begin();
		ofy.delete(u);
	}

	/* ==== SHARED ==== */
	public Folder getSharedFolder(User friend) 
	{
		Objectify ofy = ObjectifyService.begin();
		Query<Folder> p = ofy.query(Folder.class).filter("owner", friend);
		ArrayList<Folder> folders = (ArrayList<Folder>) p.list();
		
		for(Folder f : folders)
		{
			if(f.getName().equals("Shared"))
				return f;
		}
		
		return null;
	}
}
