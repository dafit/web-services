/**
 * Created by Dawid on 2015-01-24.
 */
/*global define*/
define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    'use strict';

    var File = Backbone.Model.extend({
        defaults: {
            icon: 'file',
            id: '',
            name: '',
            size: '',
            type: 'file',
            contentSize: 0
        }
    });

    return File;
});