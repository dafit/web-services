package edu.dctgkk.ws.helpers;

import com.googlecode.objectify.Key;

import edu.dctgkk.ws.dao.DBDAO;
import edu.dctgkk.ws.json.FolderJSON;
import edu.dctgkk.ws.json.ItemsJSON;
import edu.dctgkk.ws.model.Folder;
import edu.dctgkk.ws.model.Metafile;

public class JsonParser {

	public static FolderJSON generateFolderJson(Folder f) {
		DBDAO db = new DBDAO();
		FolderJSON fjson = new FolderJSON();

		fjson.setId(f.getID().toString());
		fjson.setName(f.getName());

		for (Key<Metafile> id : f.getMetafiles()) {
			Metafile mf = db.getMetafileByID(id.getId());
			if (mf != null) {
				fjson.getItems().add(generateMetafileJSON(mf, f.getName()));
			}
		}

		for (Key<Folder> id : f.getSubfolders()) {
			Folder mf = db.getFolderByID(id.getId());
			if (mf != null) {
				fjson.getItems().add(generateSubfolder(mf));
			}
		}

		return fjson;
	}

	private static ItemsJSON generateMetafileJSON(Metafile mf, String folderName) {
		ItemsJSON mfjson = new ItemsJSON();
		mfjson.setId(mf.getID());
		mfjson.setName(mf.getName());
		mfjson.setType(mf.getType());
		mfjson.setSize(mf.getSize());
		mfjson.setCreationTime(mf.getCreationTime());
		if(folderName.equals("Shared"))
		{
			mfjson.setModifiable(false);
		}
		else{
			mfjson.setModifiable(true);
		}

		return mfjson;
	}

	private static ItemsJSON generateSubfolder(Folder f) {
		ItemsJSON subfolderJson = new ItemsJSON();

		subfolderJson.setId(f.getID());
		subfolderJson.setContentSize(f.getSubfoldersSize()
				+ f.getMetafilesSize());
		subfolderJson.setName(f.getName());
		subfolderJson.setType("FOLDER");
		subfolderJson.setCreationTime(f.getCreationTime());
		subfolderJson.setModifiable(f.isModifiable());

		return subfolderJson;
	}

}
