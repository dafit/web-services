/**
 * Created by Dawid on 2015-01-30.
 */
/**
 * Created by Dawid on 2015-01-30.
 */
define([
    'bootstrap',
    'handlebars',
    'backbone',
    'text!templates/response-viewer-template.html',
    'common'
], function ($, Handlebars, Backbone, responseTemplate, Common) {
    'use strict';

    var ResponseView = Backbone.View.extend({

        tagName: 'span',


        template: Handlebars.compile(responseTemplate),
        events: {
            'click .glyphicon-arrow-right': 'moveRight',
            'click .glyphicon-arrow-left': 'moveLeft'
        },

        initialize: function () {
            this.currentIndex = 0;
        },

        render: function () {
            this.$el.html(this.template(Common.getRequests()[this.currentIndex]));
            if (this.currentIndex == 0) {
                this.$(".glyphicon-arrow-left").addClass("inactive");
            }
            return this;
        },
        moveRight: function () {
            if (this.currentIndex < Common.getRequests().length - 1) {
                this.currentIndex += 1;
                this.$(".glyphicon-arrow-left").removeClass("inactive");
                this.render();
            }
            if (this.currentIndex == Common.getRequests().length - 1) {
                this.$(".glyphicon-arrow-right").addClass("inactive");
            }
        },
        moveLeft: function () {
            if (this.currentIndex > 0) {
                this.currentIndex -= 1;
                this.render();
                this.$(".glyphicon-arrow-right").removeClass("inactive");
            }
            if (this.currentIndex == 0) {
                this.$(".glyphicon-arrow-left").addClass("inactive");
            }
        }


    });

    return ResponseView;
});