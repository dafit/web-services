/**
 * Created by Dawid on 2015-01-25.
 */

define(['pwstrength', 'backbone', 'handlebars', 'text!templates/register-template.html', 'common'], function ($, Backbone, Handlebars, registerTemplate, Common) {

    var RegisterView = Backbone.View.extend({

        template: Handlebars.compile(registerTemplate),

        el: '#main-container',

        events: {
            'click button': 'register'
        },

        initialize: function () {
            $("#navbar").hide();

            this.render();
        },

        render: function () {
            this.$el.html(this.template());
            this.$login = this.$("#input-register-login");
            this.$password = this.$("#input-register-password")
            this.$confirmPassword = this.$("#input-register-confirm-password");
            this.$submitButton = this.$("button");

            var options = {};
            options.ui = {
                container: "#register",
                showVerdicts: false,
                viewports: {
                    progress: "#pwstrength-progress"
                }
            };
            $('#input-register-password').pwstrength(options);
        },

        register: function (e) {
            e.preventDefault();

            if (this.$password.val() === this.$confirmPassword.val()) {
                var _self = this, data = {login: this.$login.val(), password: this.$password.val()},
                    success = function (data, statusCode, request) {
                        _self.goTo("login");
                    };

                $.ajax({
                    url: Common.RegisterURL,
                    method: 'POST',
                    data: JSON.stringify(data),
                    contentType: "application/json",
                    success: success,
                    error: Common.defaultError
                });
            }
        }
    });
    return RegisterView;

});
