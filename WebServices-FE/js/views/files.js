/**
 * Created by Dawid on 2015-01-24.
 */
/*global define*/
define([
    'jQuery',
    '../helpers/handlebarsHelper',
    'backbone',
    'collections/files',
    'views/file',
    'views/breadcrumbs',
    'views/uploadModal',
    'views/createFolderModal',
    'views/responseViewer',
    'text!templates/file-list-template.html',
    'common'
], function ($, Handlerbars, Backbone, Files, FileView, BreadCrumbsView, UploadModalView, CreateFolderModalView, ResponseViewer, fileListTemplate, Common) {
    'use strict';

    var AppView = Backbone.View.extend({

        el: '#main-container',

        subViewsList: [],
        currentUserData: undefined,

        template: Handlerbars.compile(fileListTemplate),

        events: {
            'click #upload-btn': 'showUploadModal',
            'click #create-btn': 'showCreateFolderdModal',
            'click .glyphicon-search': 'searchByName'
        },

        refreshCollection: function () {
            Files.fetch({reset: true, beforeSend: Common.sendAuthentication});
        },

        initialize: function () {

            $("#navbar").show();

            this.listenTo(Files, 'all', this.render);

            this.refreshCollection();
        },

        render: function () {
            this.$el.html(this.template());

            //clear old sub-views
            if (this.subViewsList && this.subViewsList.length > 0) {
                for (var i = 0, len = this.subViewsList.length; i < len; i++) {
                    this.subViewsList[i].undelegateEvents();
                    this.subViewsList[i].remove();
                }
                this.subViewsList = [];
            }
            //render folders & files table
            if (Files.length) {
                Files.each(this.renderOne, this);
            }

            //render Breadcrumbs
            var breadCrumbsView = new BreadCrumbsView({model: Common.getBreadCrumbs()});
            this.$("#breadcrumbs-container").html(breadCrumbsView.render().el);
            this.subViewsList.push(breadCrumbsView);

            //render Upload modalView
            var uploadModalView = new UploadModalView();
            this.$("#upload-modal-container").html(uploadModalView.render().el);
            this.subViewsList.push(uploadModalView);

            //render Create modalView
            var createFolderView = new CreateFolderModalView();
            this.$("#create-modal-container").html(createFolderView.render().el);
            this.subViewsList.push(createFolderView);

            //render ResponseView
            var responseViewer = new ResponseViewer();
            this.$(".response-viewer").html(responseViewer.render().el);
            this.subViewsList.push(responseViewer);
            this.loadUserData();
        },

        renderOne: function (file) {
            var view = new FileView({model: file});
            this.$('tbody').append(view.render().el);
            this.subViewsList.push(view);
        },

        showUploadModal: function () {
            $("#upload-modal-container .modal").modal("show");
        },

        showCreateFolderdModal: function () {
            $("#create-modal-container .modal").modal("show");
        },

        loadUserData: function (reload) {
            if (reload || !this.currentUserData) {
                var _self = this, success = function (response) {
                    _self.currentUserData = '<span>Hi ' + response.login + ' your storage is taken in ' + (response.storageCurrent / response.storageMax).toFixed(2) + '%<span>';
                    _self.$("#user-data").html(_self.currentUserData);
                };

                $.ajax({
                    url: Common.UsersURL + '/' + Common.getUID(),
                    method: 'GET',
                    beforeSend: Common.sendAuthentication,
                    contentType: "application/json",
                    success: success,
                    error: Common.defaultError
                });
            }
            else {
                this.$("#user-data").html(this.currentUserData);
            }
        },

        searchByName: function () {
            var name = this.$("#search-name").val();

            Files.fetch({
                url: Common.FileURL + '/?name=' + name,
                parse: function (response) {
                    return response.items;
                },
                reset: true,
                beforeSend: Common.sendAuthentication,
                success: function () {
                    Common.getBreadCrumbById(Common.getHeadId());
                    Common.pushNewBreadCrumb({id: 0, name: 'Search Results'});
                }
            });
        }

    });

    return AppView;
});