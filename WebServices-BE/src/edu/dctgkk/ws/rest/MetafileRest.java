package edu.dctgkk.ws.rest;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.gson.Gson;
import com.googlecode.objectify.Key;

import edu.dctgkk.ws.dao.DBDAO;
import edu.dctgkk.ws.model.Folder;
import edu.dctgkk.ws.model.Metafile;
import edu.dctgkk.ws.model.User;

@Path("/metafiles")
public class MetafileRest
{
    @GET
    @Produces({ "application/json" })
    @Path("{ID}")
    public Response getMetafileByID(@PathParam("ID") Long id, @Context HttpHeaders headers)
    {
        DBDAO db = new DBDAO();
        Gson g = new Gson();

        Long UID = Long.valueOf(headers.getRequestHeader("UID").get(0));
        String UToken = headers.getRequestHeader("TOKEN").get(0);

        if (db.isLoggedIn(UID, UToken))
        {
            Metafile mf = db.getMetafileByID(id);

            if (mf == null)
            {
                return Response.status(Status.NOT_FOUND).build();
            }

            return Response.ok(g.toJson(mf)).build();
        } else
            return Response.status(440).header("Message: ", "You are not logged in.").build();
    }

    @GET
    @Produces({ "application/json" })
    public Response getMetafileByName(@QueryParam("metafileName") String metafileName, @Context HttpHeaders headers)
    {
        DBDAO db = new DBDAO();
        Gson g = new Gson();

        Long UID = Long.valueOf(headers.getRequestHeader("UID").get(0));
        String UToken = headers.getRequestHeader("TOKEN").get(0);

        if (db.isLoggedIn(UID, UToken))
        {
            Metafile mf = db.getMetafileByName(metafileName);

            if (mf == null)
            {
                return Response.status(Status.NOT_FOUND).entity("No files with given name found").build();
            }

            return Response.ok(g.toJson(mf)).build();
        } else
            return Response.status(440).header("Message: ", "You are not logged in.").build();
    }

    @PUT
    @Produces({ "application/json" })
    public Response renameMetafile(@Context HttpHeaders headers, String parameter1)
    {
        DBDAO db = new DBDAO();
        Gson g = new Gson();

        Long UID = Long.valueOf(headers.getRequestHeader("UID").get(0));
        String UToken = headers.getRequestHeader("TOKEN").get(0);

        if (db.isLoggedIn(UID, UToken))
        {
            Metafile p = g.fromJson(parameter1, Metafile.class);

            if (db.isMetafileNameAvailable(p.getName()))
            {
                Metafile f = db.getMetafileByID(p.getID());
                f.setName(p.getName());
                db.postMetafile(f);
                return Response.ok(g.toJson(f)).build();
            } else
                return Response.status(400).header("Message: ", "File " + p.getName() + " already exists.").build();
        } else
            return Response.status(440).header("Message: ", "You are not logged in.").build();

    }

    @DELETE
    @Produces({ "application/json" })
    @Path("{ID}")
    public Response deleteFile(@PathParam("ID") Long id, @Context HttpHeaders headers)
    {
        DBDAO db = new DBDAO();
        Gson g = new Gson();

        Long UID = Long.valueOf(headers.getRequestHeader("UID").get(0));
        String UToken = headers.getRequestHeader("TOKEN").get(0);

        if (db.isLoggedIn(UID, UToken))
        {

            Folder folder = db.getFolderByID(Long.parseLong(headers.getRequestHeader("folderId").get(0)));

            if (folder.getName().equals("Shared"))
            {
                return Response.status(401).header("Message: ", "You are not an owner of this file.").build();
            }

            // refresh storage size
            int size = db.getMetafileByID(id).getSize();
            // User owner = db.getUser(headers.getRequestHeader("UID").get(0));
            User owner = db.getUser(UID);
            owner.setStorageCurrent(owner.getStorageCurrent() - size);
            db.postUser(owner);

            folder.getMetafiles().remove(new Key<Metafile>(Metafile.class, id));
            db.deleteFileAndMetafileById(id);
            db.postFolder(folder);

            return Response.ok(g.toJson("File deleted")).build();
        } else
            return Response.status(440).header("Message: ", "You are not logged in.").build();
    }
}
