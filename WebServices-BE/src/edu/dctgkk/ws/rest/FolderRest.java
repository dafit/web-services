package edu.dctgkk.ws.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.gson.Gson;
import com.googlecode.objectify.Key;

import edu.dctgkk.ws.dao.DBDAO;
import edu.dctgkk.ws.helpers.JsonParser;
import edu.dctgkk.ws.json.FolderCreateJSON;
import edu.dctgkk.ws.json.FolderJSON;
import edu.dctgkk.ws.model.File;
import edu.dctgkk.ws.model.Folder;
import edu.dctgkk.ws.model.Metafile;
import edu.dctgkk.ws.model.User;

@Path("/folders")
public class FolderRest
{

    @GET
    @Produces({ "application/json" })
    public Response getFolders(@Context HttpHeaders headers)
    {
        DBDAO db = new DBDAO();
        Gson g = new Gson();

        Long UID = Long.valueOf(headers.getRequestHeader("UID").get(0));
        String UToken = headers.getRequestHeader("TOKEN").get(0);

        if (db.isLoggedIn(UID, UToken))
        {
            User usr = db.getUser(UID);
            List<FolderJSON> result = new ArrayList<FolderJSON>();
            List<Folder> folders = db.getAllFolders(usr);
            for (Folder folder : folders)
            {
                result.add(JsonParser.generateFolderJson(folder));
            }
            return Response.ok(g.toJson(result)).build();
        } else
            return Response.status(440).header("Message: ", "You are not logged in.").build();
    }

    @GET
    @Produces({ "application/json" })
    @Path("{ID}")
    public Response getFolderByID(@PathParam("ID") Long id, @Context HttpHeaders headers)
    {
        // rest/folders/1 => folder.id!=1, folder.ownerId==1
        DBDAO db = new DBDAO();
        Gson g = new Gson();

        Long UID = Long.valueOf(headers.getRequestHeader("UID").get(0));
        String UToken = headers.getRequestHeader("TOKEN").get(0);

        if (db.isLoggedIn(UID, UToken))
        {
            Folder f = db.getFolderByID(id);

            if (f == null)
            {
                return Response.status(Status.NOT_FOUND).build();
            }

            if (f.getName().equals("Shared"))
            {
                boolean isContentChanged = false;
                List<Key<Metafile>> mfs = f.getMetafiles();

                /*
                 * Check if files in shared exists; delete them otherwise
                 */
                for (Key<Metafile> kmf : mfs)
                {
                    Metafile mf = db.getMetafileByID(kmf.getId());
                    if (mf != null)
                    {
                        File file = db.getFileByID(mf.getFileID());

                        if (file == null)
                        {
                            f.getMetafiles().remove(kmf);
                            isContentChanged = true;
                        }
                    }
                }

                if (isContentChanged)
                    db.postFolder(f);
            }

            return Response.ok(g.toJson(JsonParser.generateFolderJson(f))).build();

        } else
            return Response.status(440).header("Message: ", "You are not logged in.").build();
    }

    @POST
    @Produces({ "application/json" })
    public Response createFolder(@Context HttpHeaders headers, String parameter1)
    {
        DBDAO db = new DBDAO();
        Gson g = new Gson();

        Long UID = Long.valueOf(headers.getRequestHeader("UID").get(0));
        String UToken = headers.getRequestHeader("TOKEN").get(0);

        if (db.isLoggedIn(UID, UToken))
        {
            FolderCreateJSON p = g.fromJson(parameter1, FolderCreateJSON.class);
            Folder f = new Folder();
            f.setCreationTime(new Date());
            f.setModifiable(true);
            f.setName(p.getName());

            f.setOwner(new Key<User>(User.class, UID));
            db.postFolder(f);

            Folder parentfolder = db.getFolderByID(p.getParentId());

            parentfolder.getSubfolders().add(new Key<Folder>(Folder.class, f.getID()));
            db.postFolder(parentfolder);

            try
            {
                return Response.created(new URI("folders/" + f.getID())).entity(g.toJson("Folder created")).build();
            } catch (URISyntaxException e)
            {
                e.printStackTrace();
                db.deleteFolder(f);
                return Response.serverError().header("Message: ", "Server couldn't create a folder. Please try again later.").build();
            }
        } else
            return Response.status(440).header("Message: ", "You are not logged in.").build();
    }

    @PUT
    @Produces({ "application/json" })
    public Response renameFolder(@Context HttpHeaders headers, String parameter1)
    {
        DBDAO db = new DBDAO();
        Gson g = new Gson();

        Long UID = Long.valueOf(headers.getRequestHeader("UID").get(0));
        String UToken = headers.getRequestHeader("TOKEN").get(0);

        if (db.isLoggedIn(UID, UToken))
        {
            Folder p = g.fromJson(parameter1, Folder.class);

            if (db.isFolderNameAvailable(p.getName()))
            {
                Folder f = db.getFolderByID(p.getID());
                f.setName(p.getName());
                db.postFolder(f);
                return Response.ok(g.toJson(f)).build();
            } else
                return Response.status(400).header("Message: ", "Folder " + p.getName() + " already exists.").build();
        } else
            return Response.status(440).header("Message: ", "You are not logged in.").build();

    }

    @DELETE
    @Produces({ "application/json" })
    @Path("{ID}")
    public Response deleteFolder(@PathParam("ID") Long id, @Context HttpHeaders headers)
    {
        DBDAO db = new DBDAO();
        Gson g = new Gson();

        Long UID = Long.valueOf(headers.getRequestHeader("UID").get(0));
        String UToken = headers.getRequestHeader("TOKEN").get(0);

        if (db.isLoggedIn(UID, UToken))
        {
            Folder folder = db.getFolderByID(id);

            List<Key<Metafile>> mfs = folder.getMetafiles();
            List<Key<Folder>> subfolders = folder.getSubfolders();
            
            /* Delete subfolders and its contents */
            for (Key<Folder> sub : subfolders)
            {
                Folder subf = db.getFolderByID(sub.getId());          
                
                /* Delete files */
                List<Key<Metafile>>submfs =subf.getMetafiles();
                for (Key<Metafile> kmf : submfs)
                    db.deleteFileAndMetafileById(kmf.getId());
                
                db.deleteFolder(subf);
            }
            
            for (Key<Metafile> kmf : mfs)
                db.deleteFileAndMetafileById(kmf.getId());

            db.deleteFolder(folder);

            return Response.ok(g.toJson("Folder deleted!")).build();
        } else
            return Response.status(440).header("Message: ", "You are not logged in.").build();
    }
}
