package edu.dctgkk.ws.json;

import java.util.ArrayList;
import java.util.List;

public class FolderJSON {
	String id;
	String name;
	List<ItemsJSON> items;

	public FolderJSON() {
		items = new ArrayList<ItemsJSON>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ItemsJSON> getItems() {
		return items;
	}
}
