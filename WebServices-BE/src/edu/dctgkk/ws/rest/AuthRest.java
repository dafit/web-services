package edu.dctgkk.ws.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.googlecode.objectify.Key;

import edu.dctgkk.ws.dao.DBDAO;
import edu.dctgkk.ws.model.Folder;
import edu.dctgkk.ws.model.User;

@Path("/auth")
public class AuthRest {
	@POST
	@Produces({ "application/json" })
	@Path("login")
	public Response login(String parameter1) {
		DBDAO db = new DBDAO();
		Gson g = new Gson();

		User u = g.fromJson(parameter1, User.class);
		String password = sha256(u.getPassword());

		if (db.isPasswordCorrect(u.getLogin(), password)) {
			User u2 = db.getUser(u.getLogin());

			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			c.add(Calendar.HOUR, 1);
			u2.setUserSessionExpirationDate(c.getTime());

			Random r = new Random();
			u2.setToken(Integer.toString(r.nextInt(Integer.MAX_VALUE / 2 - 1)
					+ Integer.MAX_VALUE / 2 - 1));

			db.postUser(u2);
			return Response.ok(g.toJson("Login Success"))
					.header("Token", u2.getToken()).header("UID", u2.getID())
					.header("HeadId", u2.getHeadFolder().getId()).build();
		} else
			return Response.status(403)
					.entity(g.toJson("Message : Bad login or password."))
					.build();
	}

	@POST
	@Path("register")
	public Response register(String parameter1) {
		DBDAO db = new DBDAO();
		Gson g = new Gson();

		User u = g.fromJson(parameter1, User.class);
		u.setStorageCurrent(0);
		u.setStorageMax(10000000); // 10MB

		if (db.isLoginAvailable(u.getLogin())) {
			u.setPassword(sha256(u.getPassword()));
			db.postUser(u);

			/* Create HEAD folder and Shared folder */
			Folder shared = new Folder();
			shared.setName("Shared");
			shared.setOwner(new Key<User>(User.class, u.getID()));
			shared.setModifiable(false);
			db.postFolder(shared);

			Folder head = new Folder();
			head.setName("HEAD");
			head.setOwner(new Key<User>(User.class, u.getID()));
			head.setModifiable(false);
			head.getSubfolders().add(
					new Key<Folder>(Folder.class, shared.getID()));
			db.postFolder(head);

			u.setHeadFolder(new Key<Folder>(Folder.class, head.getID()));
			db.postUser(u);

			try {
				return Response.created(new URI("users/" + u.getID())).build();
			} catch (URISyntaxException e) {
				e.printStackTrace();
				db.deleteUser(u);
				return Response
						.serverError()
						.entity("Message: Server couldn't create an account. Please try again later.")
						.build();
			}
		} else
			return Response.status(409)
					.entity("Message: User with a given name already exists.")
					.build();
	}

	public String sha256(String base) {
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(base.getBytes("UTF-8"));
			StringBuffer hexString = new StringBuffer();

			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xff & hash[i]);
				if (hex.length() == 1)
					hexString.append('0');
				hexString.append(hex);
			}

			return hexString.toString();
		} catch (Exception ex) {
			return null;
		}
	}
}
