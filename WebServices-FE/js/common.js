/**
 * Created by Dawid on 2015-01-24.
 */
define([], function () {
    var local = false, breadcrumbsList = [],
        requestList = [], showAlert = function (text, type) {
            document.getElementById('notifications-container').innerHTML = "<div class='alert alert-" + type +
            " alert-dismissable'><button type='button' class='close' data-dismiss='aler'>×</button>" + text +
            "</div>";

            setTimeout(function () {
                document.getElementById('notifications-container').innerHTML = '';
            }, 5000);
        }, AlertTypes = {danger: 'danger', warning: 'warning', success: 'success', info: 'info'};

    return {
        LoginURL: local ? 'http://localhost:1234/rest/auth/login' : 'http://2-dot-web-services-file-box.appspot.com/rest/auth/login',
        RegisterURL: local ? 'http://localhost:1234/rest/auth/register' : 'http://2-dot-web-services-file-box.appspot.com/rest/auth/register',
        FolderURL: local ? 'http://localhost:1234/rest/folders' : 'http://2-dot-web-services-file-box.appspot.com/rest/folders',
        FilesUploadURL: local ? 'http://localhost:1234/rest/files/upload' : 'http://2-dot-web-services-file-box.appspot.com/rest/files/upload',
        FilesDownloadURL: local ? 'http://localhost:1234/rest/files/download/' : 'http://2-dot-web-services-file-box.appspot.com/rest/files/download/',
        UsersURL: local ? 'http://localhost:1234/rest/users' : 'http://2-dot-web-services-file-box.appspot.com/rest/users',
        FileURL: local ? 'http://localhost:1234/rest/metafiles' : 'http://2-dot-web-services-file-box.appspot.com/rest/metafiles',
        ShareURL: local ? 'http://localhost:1234/rest/share/' : 'http://2-dot-web-services-file-box.appspot.com/rest/share/',

        maxFileSize: 950000,
        setToken: function (token) {
            sessionStorage.setItem("token", token);
        },
        getToken: function () {
            return sessionStorage.getItem("token");
        },
        setUID: function (uid) {
            sessionStorage.setItem("UID", uid);
        },
        getUID: function () {
            return sessionStorage.getItem("UID");
        },
        setHeadId: function (uid) {
            sessionStorage.setItem("HeadId", uid);
        },
        getHeadId: function () {
            if (breadcrumbsList && breadcrumbsList.length > 0) {
                return breadcrumbsList[breadcrumbsList.length - 1].id;
            }
            else {
                this.pushNewBreadCrumb({name: 'HEAD', id: sessionStorage.getItem("HeadId")});
                return sessionStorage.getItem("HeadId");
            }
        },
        clearUserData: function () {
            sessionStorage.removeItem("UID");
            sessionStorage.removeItem("token");
            sessionStorage.removeItem("HeadId");
            breadcrumbsList = [];
        },
        getBreadCrumbById: function (id) {
            var result = breadcrumbsList.filter(function (item) {
                return item.id === id;
            }), index = breadcrumbsList.indexOf(result[0]);
            if (index > -1) {
                breadcrumbsList.splice(index + 1, breadcrumbsList.length - 1);
            }
        },
        pushNewBreadCrumb: function (breadCrumb) {
            var result = breadcrumbsList.filter(function (item) {
                return item.id === breadCrumb.id;
            });
            if (result && result.length > 0) {
                console.warn("This folder is already pushed!");
            }
            else {
                breadcrumbsList.push(breadCrumb);
            }
        },
        getBreadCrumbs: function () {
            return breadcrumbsList;
        },
        sendAuthentication: function (xhr) {
            xhr.setRequestHeader('UID', sessionStorage.getItem("UID"));
            xhr.setRequestHeader('TOKEN', sessionStorage.getItem("token"));
        },
        showAlert: showAlert,
        AlertTypes: AlertTypes,
        afterSend: function () {
            requestList.push({
                response: this.response,
                url: this.responseURL,
                status: this.status,
                statusText: this.statusText
            });
            if (this.status != 200 && this.status != 201) {
                showAlert("Error occured inspect the response Viewer, when you are logged in.", AlertTypes.danger);
            }
        },
        getRequests: function () {
            return requestList;
        },
        defaultError: function () {
            showAlert("Error occured inspect the response Viewer, when you are logged in.", AlertTypes.danger);
        }


    };
});