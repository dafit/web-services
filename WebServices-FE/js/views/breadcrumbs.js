/**
 * Created by Dawid on 2015-01-30.
 */
define([
    'jQuery',
    'handlebars',
    'backbone',
    'text!templates/breadcrumb-template.html'
], function ($, Handlebars, Backbone, breadCrumbTemplate) {
    'use strict';

    var BreadCrumbsView = Backbone.View.extend({

        tagName:  'span',

        template: Handlebars.compile(breadCrumbTemplate),

        initialize: function () {

        },

        render: function () {
            this.$el.html(this.template(this.model));
            return this;
        }
    });

    return BreadCrumbsView;
});