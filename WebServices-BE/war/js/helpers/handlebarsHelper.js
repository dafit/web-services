/**
 * Created by Dawid on 2015-01-24.
 */

define(['handlebars'], function (Handlebars) {

    var extendedHandlebars = Handlebars;
    extendedHandlebars.registerHelper("iconHelper", function (data) {
        if (data === "FOLDER") {
            return "glyphicon-folder-open";
        }
        else {
            return "glyphicon-file";
        }
    });

    extendedHandlebars.registerHelper("badge", function (data) {
        return new Handlebars.SafeString(" <span class=\'badge\'>" + data + "</span>");
    });

    extendedHandlebars.registerHelper("safe", function (data) {
        return new Handlebars.SafeString(data);
    });

    extendedHandlebars.registerHelper('isFolder', function (val, options) {
        var fnTrue = options.fn, fnFalse = options.inverse;
        return val === "FOLDER" ? fnTrue() : fnFalse();
    });

    extendedHandlebars.registerHelper('escape', function (val) {
        if (val) {
            var obj = JSON.parse(val),
                result = JSON.stringify(obj, null, 4);
            return new Handlebars.SafeString(result);
        }
        else {
            return '';
        }
    });

    return extendedHandlebars;

});
