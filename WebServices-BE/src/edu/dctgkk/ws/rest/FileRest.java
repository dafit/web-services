package edu.dctgkk.ws.rest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.googlecode.objectify.Key;

import edu.dctgkk.ws.dao.DBDAO;
import edu.dctgkk.ws.json.FileJSON;
import edu.dctgkk.ws.model.File;
import edu.dctgkk.ws.model.Folder;
import edu.dctgkk.ws.model.Metafile;
import edu.dctgkk.ws.model.User;

@Path("/files")
public class FileRest {
	@GET
	@Produces({ "application/json" })
	@Path("download/{ID}")
	public Response download(@Context HttpHeaders headers,
			@PathParam("ID") Long id) {
		DBDAO db = new DBDAO();
		Gson g = new Gson();

		Long UID = Long.valueOf(headers.getRequestHeader("UID").get(0));
		String UToken = headers.getRequestHeader("TOKEN").get(0);

		if (db.isLoggedIn(UID, UToken)) {
			Metafile mf2 = db.getMetafileByID(id);

			FileJSON fj = new FileJSON();
			fj.setData(db.getFileByID(mf2.getFileID()).getData());
			fj.setType(mf2.getType());
			fj.setName(mf2.getName());
			return Response.ok(g.toJson(fj)).build();
		} else
			return Response.status(440)
					.header("Message: ", "You are not logged in.").build();
	}

	@POST
	@Produces({ "application/json" })
	@Path("upload")
	public Response upload(@Context HttpHeaders headers, String parameter1) {
		DBDAO db = new DBDAO();
		Gson g = new Gson();

		Long UID = Long.valueOf(headers.getRequestHeader("UID").get(0));
		String UToken = headers.getRequestHeader("TOKEN").get(0);

		if (db.isLoggedIn(UID, UToken)) {
			FileJSON fjson = g.fromJson(parameter1, FileJSON.class);
			User u = db.getUser(UID);

			if(fjson.getSize() + u.getStorageCurrent() > u.getStorageMax())
			{
				return Response.status(413).header("Message: ", "You don't have enough space to store this file.").build();
			}
			else
			{
				File f = new File(fjson.getData());
				db.postFile(f);
	
				Metafile mf = new Metafile();
				mf.setCreationTime(fjson.getCreationTime());
				mf.setFileID(f.getID());
				mf.setName(fjson.getName());
				mf.setSize(fjson.getSize());
				mf.setType(fjson.getType());
				db.postMetafile(mf);
	
				// add file to a folder
				Folder folder = db.getFolderByID(fjson.getFolderID());
				folder.getMetafiles().add(
						new Key<Metafile>(Metafile.class, mf.getID()));
				db.postFolder(folder);
				
				u.setStorageCurrent(fjson.getSize() + u.getStorageCurrent());
				db.postUser(u);
	
				return Response.ok(g.toJson("Message: File saved successfully!"))
						.build();
			}
		} else
			return Response.status(440).header("Message: ", "You are not logged in.").build();
	}


}
