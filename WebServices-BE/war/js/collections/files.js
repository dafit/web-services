/**
 * Created by Dawid on 2015-01-24.
 */
/*global define */
define([
    'underscore',
    'backbone',
    'models/file',
    'common'
], function (_, Backbone, File, Common) {
    'use strict';

    var FilesCollection = Backbone.Collection.extend({
        model: File,
        url: function () {
            return Common.FolderURL + "/" + Common.getHeadId();
        },
        parse: function (response) {
            return response.items;
        }

    });

    return new FilesCollection();
});