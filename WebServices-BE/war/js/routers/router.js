/**
 * Created by Dawid on 2015-01-24.
 */

define([
    'jQuery',
    'backbone',
    'common',
    'views/files',
    'views/register',
    'views/login'
], function ($, Backbone, Common, FilesView, RegisterView, LoginView) {
    'use strict';

    var currentView;

    var cleanAndAuthenticate = function (skipAuthenticate) {
        if (currentView) {
            currentView.close();
            currentView.undelegateEvents();
            currentView=null;
        }
        if (!skipAuthenticate) {
            return (Common.getToken() && Common.getToken().length > 0);
        }
    };

    var routeManager = {
        goToFiles: function (param) {
            if (cleanAndAuthenticate(false)) {
                if (param) {
                    Common.getBreadCrumbById(param);
                }
                currentView = new FilesView();
            }
            else {
                Common.clearUserData();
                Backbone.history.navigate("login", {trigger: true});
            }
        },

        refreshFiles: function () {
            if (currentView instanceof FilesView) {
                currentView.loadUserData(true);
                currentView.refreshCollection();
            }
            Backbone.history.navigate('', {trigger: true});

        },

        //authentication Part

        register: function () {
            cleanAndAuthenticate(true);
            currentView = new RegisterView();
        },
        login: function () {
            cleanAndAuthenticate(true);
            currentView = new LoginView();
        },
        logout: function () {
            Common.clearUserData();
            Backbone.history.navigate("login", {trigger: true});
        }

    };

    var FileRouter = Backbone.Router.extend({
        routes: {
            '': 'main',
            'files*': 'files',
            'register': 'register',
            'login': 'login',
            'logout': 'logout',
            'refreshFiles': 'refreshFiles'
        },
        main: routeManager.goToFiles,
        files: routeManager.goToFiles,
        refreshFiles: routeManager.refreshFiles,
        register: routeManager.register,
        login: routeManager.login,
        logout: routeManager.logout
    });

    return FileRouter;
});
