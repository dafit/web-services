package edu.dctgkk.ws.model;

import javax.persistence.Id;

import com.googlecode.objectify.annotation.Entity;

@Entity
public class File {

	@Id
	Long ID = null;
	String data;
	
	public File() {}
	
	public File(String data) {
		super();
		this.data = data;
	}
	
	public Long getID() {
		return ID;
	}
	public void setID(Long iD) {
		ID = iD;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	
}
