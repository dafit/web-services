package edu.dctgkk.ws.rest;

import java.util.List;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.googlecode.objectify.Key;

import edu.dctgkk.ws.dao.DBDAO;
import edu.dctgkk.ws.model.Folder;
import edu.dctgkk.ws.model.Metafile;
import edu.dctgkk.ws.model.User;

@Path("/share")
public class ShareRest
{
    @POST
    @Produces({ "application/json" })
    @Path("{ID}")
    // param - metafile
    public Response getFolders(@PathParam("ID") Long id, @Context HttpHeaders headers, String param)
    {
        DBDAO db = new DBDAO();
        Gson g = new Gson();

        Long UID = Long.valueOf(headers.getRequestHeader("UID").get(0));
        String UToken = headers.getRequestHeader("TOKEN").get(0);

        if (db.isLoggedIn(UID, UToken))
        {
            User friend = db.getUser(id);
            Metafile mf = g.fromJson(param, Metafile.class);

            Folder folder = db.getSharedFolder(friend);

            if (folder == null)
            {
                return Response.status(404).header("Message: ", "Shared folder not found.").build();
            }
            
            boolean isFound = false;
            List<Key<Metafile>> mfs = folder.getMetafiles();

            for (Key<Metafile> kmf : mfs)
            {
                Metafile metaf = db.getMetafileByID(kmf.getId());
                if(metaf != null)
                {
                    if (metaf.getID() == mf.getID())
                    {
                        isFound = true;
                        break;
                    }
                }
            }

            if (!isFound)
            {
                folder.getMetafiles().add(new Key<Metafile>(Metafile.class, mf.getID()));
                db.postFolder(folder);
                return Response.ok(g.toJson("File shared!")).build();
            }

            return Response.status(409).header("Message: ", "File already shared!").build();

        } else
            return Response.status(440).header("Message: ", "You are not logged in.").build();
    }
}
