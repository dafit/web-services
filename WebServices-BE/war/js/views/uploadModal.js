/**
 * Created by Dawid on 2015-01-30.
 */
/**
 * Created by Dawid on 2015-01-30.
 */
define([
    'bootstrap',
    'handlebars',
    'backbone',
    'text!templates/upload-modal-template.html',
    'common'
], function ($, Handlebars, Backbone, modalTemplate, Common) {
    'use strict';

    var UploadModalView = Backbone.View.extend({

        tagName: 'span',

        template: Handlebars.compile(modalTemplate),
        events: {
            'click #submit-files': 'submitFiles',
            'click #add-files': 'addFile'
        },

        initialize: function () {

        },

        render: function () {
            this.$el.html(this.template());
            this.$filesInput = this.$("#files")[0];
            return this;
        },

        addFile: function () {
            this.$filesInput.click();
        },
        submitFiles: function () {
            if (this.$filesInput && this.$filesInput.files && this.$filesInput.files.length > 0) {
                var reader = new FileReader(),
                    xhr = new XMLHttpRequest(),
                    name = '', size = '', type = '';
                this.xhr = xhr;

                var _self = this;
                this.xhr.upload.addEventListener("progress", function (e) {
                    if (e.lengthComputable) {
                        var percentage = Math.round((e.loaded * 100) / e.total);
                        console.log(percentage);
                    }
                }, false);


                xhr.open("POST", Common.FilesUploadURL);

                xhr.setRequestHeader('UID', Common.getUID());
                xhr.setRequestHeader('TOKEN', Common.getToken());
                xhr.setRequestHeader("Content-type", "application/json");

                xhr.onreadystatechange = function (response) {
                    _self.goTo('refreshFiles');
                };

                reader.onload = function (evt) {
                    debugger;
                    var rawData = new Uint8Array(evt.target.result),
                        data = base64EncArr(rawData);

                    _self.xhr.send(JSON.stringify({
                        data: data,
                        name: name,
                        type: type,
                        folderID: Common.getHeadId(),
                        size: size
                    }));
                };
                type = this.$filesInput.files[0].type;
                size = this.$filesInput.files[0].size;
                name = this.$filesInput.files[0].name;
                if (size < Common.maxFileSize) {
                    reader.readAsArrayBuffer(this.$filesInput.files[0]);
                }
                else {
                    Common.showAlert("The file is to big", Common.AlertTypes.danger);
                }

            }
        }
    });

    return UploadModalView;
});