/**
 * Created by Dawid on 2015-01-30.
 */
/**
 * Created by Dawid on 2015-01-30.
 */
define([
    'bootstrap',
    'handlebars',
    'backbone',
    'text!templates/change-modal-template.html',
    'common'
], function ($, Handlebars, Backbone, modalTemplate, Common) {
    'use strict';

    var ChangeNameModalView = Backbone.View.extend({

        tagName: 'span',

        template: Handlebars.compile(modalTemplate),
        events: {
            'click #change-name': 'changeName'
        },

        initialize: function () {

        },

        render: function () {
            this.$el.html(this.template(this.model));
            this.$nameInput = this.$("#name")[0];
            return this;
        },
        changeName: function(){
            this.trigger('changeNameSubmit',this.$nameInput.value);
        }

    });

    return ChangeNameModalView;
});