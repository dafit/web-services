package edu.dctgkk.ws.model;

import java.util.Date;

import javax.persistence.Id;

import com.googlecode.objectify.annotation.Entity;

@Entity
public class Metafile 
{
	@Id
	Long ID = null;
	Long fileID;
	String name;
	int size;
	String type;

	Date creationTime;

	public Metafile() {
		this.creationTime = new Date();
	}

	public Metafile(Long fileID, String name, int size, String type,
			Date creationTime, Date modificationTime) {
		super();
		this.fileID = fileID;
		this.name = name;
		this.size = size;
		this.type = type;
		this.creationTime = new Date();
	}

	public Long getFileID() {
		return fileID;
	}

	public void setFileID(Long fileID) {
		this.fileID = fileID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public Long getID() {
		return ID;
	}
}
