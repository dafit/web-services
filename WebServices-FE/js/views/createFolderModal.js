/**
 * Created by Dawid on 2015-01-30.
 */
/**
 * Created by Dawid on 2015-01-30.
 */
define([
    'bootstrap',
    'handlebars',
    'backbone',
    'text!templates/create-folder-modal-template.html',
    'common'
], function ($, Handlebars, Backbone, modalTemplate, Common) {
    'use strict';

    var CreateFolderModalView = Backbone.View.extend({

        tagName: 'span',

        template: Handlebars.compile(modalTemplate),
        events: {
            'click #submit-folder': 'submitFolder'
        },

        initialize: function () {

        },

        render: function () {
            this.$el.html(this.template());
            this.$folderInput = this.$("#folder-name")[0];
            return this;
        },
        sendAuthentication: function (xhr) {
            xhr.setRequestHeader('UID', Common.getUID());
            xhr.setRequestHeader('TOKEN', Common.getToken());
        },

        submitFolder: function () {
            if (this.$folderInput && this.$folderInput.value) {
                var _self = this, data = {name: this.$folderInput.value, parentId: Common.getHeadId()},
                    success = function (data, statusCode, request) {
                        _self.goTo('refreshFiles');
                    };


                $.ajax({
                    url: Common.FolderURL,
                    method: 'POST',
                    beforeSend: this.sendAuthentication,
                    data: JSON.stringify(data),
                    contentType: "application/json",
                    success: success,
                    error: Common.defaultError
                });
            }
        }
    });

    return CreateFolderModalView;
});