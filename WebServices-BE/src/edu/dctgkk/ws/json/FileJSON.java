package edu.dctgkk.ws.json;

import java.util.Date;

import javax.persistence.Id;

public class FileJSON 
{
	@Id
	Long ID = null;
	String name;
	int size;
	String type;
	Long folderID;

	Date creationTime;
	String data;

	public FileJSON() {
		this.creationTime = new Date();
	}

	public FileJSON(String name, int size, String type, Date creationTime, Date modificationTime) 
	{
		super();
		this.name = name;
		this.size = size;
		this.type = type;
		this.creationTime = new Date();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public Long getID() {
		return ID;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Long getFolderID() {
		return folderID;
	}

	public void setFolderID(Long folderID) {
		this.folderID = folderID;
	}
}
