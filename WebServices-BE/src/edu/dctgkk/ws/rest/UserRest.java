package edu.dctgkk.ws.rest;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.google.gson.Gson;

import edu.dctgkk.ws.dao.DBDAO;
import edu.dctgkk.ws.model.User;

@Path("/users")
public class UserRest
{

    @GET
    @Produces({ "application/json" })
    public Response getUsers(String parameter1, @Context Request request)
    {
        DBDAO db = new DBDAO();
        Gson g = new Gson();
        List<User> users = db.getAllUsers();
        Map<String, String> userNames = new HashMap<String, String>();

        for (User u : users)
            userNames.put(String.valueOf(u.getID()), u.getLogin());

        String ss = g.toJson(userNames);

        CacheControl cc = CacheControl.valueOf("public, max-age=30");

        EntityTag etag = new EntityTag(Integer.toString(ss.hashCode()));
        ResponseBuilder builder = request.evaluatePreconditions(etag);

        // cached resource did change -> serve updated content
        if (builder == null)
        {
            builder = Response.ok(ss);
            builder.tag(etag);
        }

        builder.cacheControl(cc);
        return builder.lastModified(new Date()).build();
    }

    @GET
    @Produces({ "application/json" })
    @Path("{ID}")
    public Response getUser(@PathParam("ID") Long id, @Context HttpHeaders headers)
    {
        DBDAO db = new DBDAO();
        Gson g = new Gson();

        Long UID = Long.valueOf(headers.getRequestHeader("UID").get(0));
        String UToken = headers.getRequestHeader("TOKEN").get(0);

        if (db.isLoggedIn(UID, UToken))
            return Response.ok(g.toJson(db.getUser(id))).build();
        else
            return Response.status(440).header("Message: ", "You are not logged in.").build();
    }

    @PUT
    @Produces({ "application/json" })
    @Path("{ID}")
    public Response putUser(@PathParam("ID") Long id, @Context HttpHeaders headers)
    {
        DBDAO db = new DBDAO();
        Gson g = new Gson();

        Long UID = Long.valueOf(headers.getRequestHeader("UID").get(0));
        String UToken = headers.getRequestHeader("TOKEN").get(0);

        if (db.isLoggedIn(UID, UToken))
        {
            User u = db.getUser(UID);
            User userToBeChanged = db.getUser(id);
            userToBeChanged.setLogin(u.getLogin());
            userToBeChanged.setPassword(u.getPassword());

            db.postUser(userToBeChanged);

            return Response.ok(g.toJson(userToBeChanged)).build();
        } else
            return Response.status(440).header("Message: ", "You are not logged in.").build();
    }

    @DELETE
    @Produces({ "application/json" })
    @Path("{ID}")
    public Response deleteUser(@PathParam("ID") Long id, @Context HttpHeaders headers)
    {
        DBDAO db = new DBDAO();

        Long UID = Long.valueOf(headers.getRequestHeader("UID").get(0));
        String UToken = headers.getRequestHeader("TOKEN").get(0);

        if (db.isLoggedIn(UID, UToken))
        {
            User userToBeDeleted = db.getUser(id);
            db.deleteUser(userToBeDeleted);

            return Response.status(200).header("Message: ", "User " + userToBeDeleted.getLogin() + " deleted successfully.").build();
        } else
            return Response.status(440).header("Message: ", "You are not logged in.").build();
    }
}
