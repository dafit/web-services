package edu.dctgkk.ws.model;

import java.util.Date;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;

@Entity
public class User 
{
	@Id
	Long ID = null;
	String login;
	String password;
	int storageCurrent;
	int storageMax;
	Key<Folder> headFolder;
	
	String token = null;
	Date userSessionExpirationDate = null;
	
	public User() {}
	
	public User(String login, String password, int storageCurrent, int storageMax) 
	{
		super();
		this.login = login;
		this.password = password;
		this.storageCurrent = storageCurrent;
		this.storageMax = storageMax;
	}

	public String getLogin() 
	{
		return login;
	}
	
	public void setLogin(String login) 
	{
		this.login = login;
	}
	
	public String getPassword() 
	{
		return password;
	}
	
	public void setPassword(String password) 
	{
		this.password = password;
	}
	
	public int getStorageCurrent() 
	{
		return storageCurrent;
	}
	
	public void setStorageCurrent(int storageCurrent) 
	{
		this.storageCurrent = storageCurrent;
	}
	
	public int getStorageMax() 
	{
		return storageMax;
	}
	
	public void setStorageMax(int storageMax) 
	{
		this.storageMax = storageMax;
	}

	public long getID() {
		return ID;
	}

	public void setID(long iD) {
		ID = iD;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getUserSessionExpirationDate() {
		return userSessionExpirationDate;
	}

	public void setUserSessionExpirationDate(Date userSessionExpirationDate) {
		this.userSessionExpirationDate = userSessionExpirationDate;
	}
	
	public Key<Folder> getHeadFolder() {
		return headFolder;
	}

	public void setHeadFolder(Key<Folder> headFolder) {
		this.headFolder = headFolder;
	}
}
