package edu.dctgkk.ws.helpers;

public class FileFolder 
{
	Long metaFileID;
	Long folderID;
	
	public FileFolder() {}
	
	public Long getMetaFileID() {
		return metaFileID;
	}

	public void setMetaFileID(Long metaFileID) {
		this.metaFileID = metaFileID;
	}

	public Long getFolderID() {
		return folderID;
	}
	
	public void setFolderID(Long folderID) {
		this.folderID = folderID;
	}
	
	
}
