/**
 * Created by Dawid on 2015-01-30.
 */
/**
 * Created by Dawid on 2015-01-30.
 */
define([
    'bootstrap',
    'handlebars',
    'backbone',
    'text!templates/share-modal-template.html',
    'common'
], function ($, Handlebars, Backbone, modalTemplate, Common) {
    'use strict';

    var ShareModalView = Backbone.View.extend({

        tagName: 'span',

        template: Handlebars.compile(modalTemplate),
        events: {
            'click #submit-share': 'shareSubmit'
        },

        initialize: function () {

        },

        render: function () {

            var users = [];
            users.push("<h4>Please select friend and press submit to share file.</h4><select id='users' multiple class='chosen-select'>");

            for (var key in this.model) {
                users.push("<option id='" + key + "'>" + this.model[key] + '</option>');
            }
            users.push('</select>');
            this.$el.html(this.template());
            this.$el.find('.modal-body').html(users.join(''));
            this.$usersSelect = this.$(".chosen-select");

            return this;
        },
        shareSubmit: function () {
            debugger;
            var result = [];
            $(this.$usersSelect).find('option:selected').each(function (i, item) {
                result.push($(item).attr('id'));
            });
            this.trigger('shareSubmit', result);
        }

    });

    return ShareModalView;
});