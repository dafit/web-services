/**
 * Created by Dawid on 2015-01-24.
 */
/*global define*/
define([
    'chosen',
    'handlebars',
    'backbone',
    'text!templates/file-template.html',
    'common',
    'views/changeNameModal',
    'views/shareModal'
], function ($, Handlebars, Backbone, fileTemplate, Common, ChangeNameModalView, ShareModalView) {
    'use strict';

    var FileView = Backbone.View.extend({

        tagName: 'tr',

        template: Handlebars.compile(fileTemplate),

        // The DOM events specific to an item.
        events: {
            'click .glyphicon-remove': 'removeEntity',
            'click .glyphicon-share': 'share',
            'click .glyphicon-pencil': 'showEditFile',
            'click .glyphicon-arrow-down': 'download',
            'click .name': 'goToFolder'
        },

        initialize: function () {
            this.listenTo(this.model, 'change', this.render);
        },

        // Re-render the titles of the todo item.
        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            this.$(".input-group").hide();
            return this;
        },

        goToFolder: function () {
            Common.pushNewBreadCrumb({name: this.model.toJSON().name, id: this.model.toJSON().id + ''});
            this.goTo("files?" + this.model.toJSON().id);
        },

        showEditFile: function () {
            this.subview = new ChangeNameModalView({model: this.model.toJSON().name});
            $("#change-modal-container").html(this.subview.render().el);
            $("#change-modal-container .modal").modal('show');
            this.listenTo(this.subview, 'changeNameSubmit', this.changeName);
        },

        share: function () {

            var _self = this, success = function (response, statusCode, request) {
                    debugger;
                    _self.subview = new ShareModalView({model: response});
                    $("#share-modal-container").html(_self.subview.render().el);
                    $(".chosen-select").chosen({width: '300px'});
                    $("#share-modal-container .modal").modal('show');
                    _self.listenTo(_self.subview, 'shareSubmit', _self.sendShare);
                };

            $.ajax({
                url: Common.UsersURL,
                method: 'GET',
                beforeSend: Common.sendAuthentication,
                contentType: "application/json",
                success: success,
                error: Common.defaultError
            });

        },

        sendShare: function (users) {
            debugger;
            if (users && users.length > 0) {
                for (var i = 0, len = users.length; i < len; i++) {

                    var _self = this, success = function (response, statusCode, request) {
                            debugger;
                            if (_self.subview) {
                                _self.subview.undelegateEvents();
                                _self.subview.remove();
                                _self.subview = null;
                            }
                            _self.goTo('refreshFiles');
                        };

                    $.ajax({
                        url: Common.ShareURL + users[i],
                        method: 'POST',
                        beforeSend: Common.sendAuthentication,
                        contentType: "application/json",
                        success: success,
                        error: Common.defaultError,
                        data: JSON.stringify({ID: _self.model.toJSON().id})
                    });
                }
            }
        },

        changeName: function (name) {
            if (name) {
                var _self = this, data = {
                        name: name,
                        ID: this.model.toJSON().id
                    }, success = function (response, statusCode, request) {
                        debugger;
                        _self.subview.undelegateEvents();
                        _self.subview.remove();
                        _self.subview = null;
                        _self.goTo('refreshFiles');
                    };

                $.ajax({
                    url: this.model.toJSON().type === 'FOLDER' ? Common.FolderURL : Common.FileURL,
                    method: 'PUT',
                    beforeSend: Common.sendAuthentication,
                    contentType: "application/json",
                    success: success,
                    error: Common.defaultError,
                    data: JSON.stringify(data)
                });
            }
        },

        download: function () {
            var success = function (response, statusCode, request) {
                    var blob = new Blob([base64DecToArr(response.data).buffer], {
                        type: response.type
                    });

                    var downloadUrl = URL.createObjectURL(blob);
                    var a = document.createElement("a");
                    a.href = downloadUrl;
                    a.download = response.name;
                    document.body.appendChild(a);
                    a.click();
                };

            $.ajax({
                url: Common.FilesDownloadURL + this.model.toJSON().id,
                method: 'GET',
                beforeSend: Common.sendAuthentication,
                contentType: "application/json",
                success: success,
                error: Common.defaultError
            });
        },

        removeEntity: function () {
            var _self = this, success = function (response, statusCode, request) {
                    debugger;
                    _self.remove();
                };
            if (this.model.toJSON().type === 'FOLDER') {
                $.ajax({
                    url: Common.FolderURL + '/' + this.model.toJSON().id,
                    method: 'DELETE',
                    beforeSend: Common.sendAuthentication,
                    contentType: "application/json",
                    success: success,
                    error: Common.defaultError
                });
            }
            else {
                $.ajax({
                    url: Common.FileURL + '/' + this.model.toJSON().id,
                    method: 'DELETE',
                    beforeSend: function (xhr) {
                        Common.sendAuthentication(xhr);
                        xhr.setRequestHeader('folderId', Common.getHeadId());
                    },
                    contentType: "application/json",
                    success: success,
                    error: Common.defaultError
                });
            }
        }
    });

    return FileView;
})
;